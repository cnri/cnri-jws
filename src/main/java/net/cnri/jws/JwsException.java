package net.cnri.jws;

public class JwsException extends Exception {
    public JwsException(String message, Throwable t) {
        super(message, t);
    }

    public JwsException(String message) {
        super(message);
    }
}
