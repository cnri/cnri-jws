package net.cnri.jws;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class JsonWebSignatureJsonSerialization {
    String payload;
    List<JsonWebSignatureSignatureJsonSerialization> signatures;

    public static class JsonWebSignatureSignatureJsonSerialization {
        @SerializedName("protected")
        String protectedPart;
        String signature;
    }
}
