package net.cnri.jws;

import java.security.PrivateKey;

public class JsonWebSignatureFactoryImpl extends JsonWebSignatureFactory {

    @Override
    public JsonWebSignature create(String payload, PrivateKey privateKey) throws JwsException {
        return new JsonWebSignatureImpl(payload, privateKey);
    }

    @Override
    public JsonWebSignature create(byte[] payload, PrivateKey privateKey) throws JwsException {
        return new JsonWebSignatureImpl(payload, privateKey);
    }

    @Override
    public JsonWebSignature deserialize(String serialization) throws JwsException {
        return new JsonWebSignatureImpl(serialization);
    }

}
