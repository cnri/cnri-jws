package net.cnri.jws;

import java.security.PublicKey;

public interface JsonWebSignature {
    String getPayloadAsString();

    byte[] getPayloadAsBytes();

    boolean validates(PublicKey publicKey) throws JwsException;

    String serialize();

    String serializeToJson();
}
