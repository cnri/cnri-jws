package net.cnri.jws;

import java.security.PrivateKey;

public abstract class JsonWebSignatureFactory {
    abstract public JsonWebSignature create(String payload, PrivateKey privateKey) throws JwsException;

    abstract public JsonWebSignature create(byte[] payload, PrivateKey privateKey) throws JwsException;

    abstract public JsonWebSignature deserialize(String serialization) throws JwsException;

    private static final JsonWebSignatureFactory INSTANCE = new JsonWebSignatureFactoryImpl();

    public static JsonWebSignatureFactory getInstance() {
        return INSTANCE;
    }
}
