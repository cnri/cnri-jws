package net.cnri.jws;

import java.nio.charset.StandardCharsets;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;

import org.apache.commons.codec.binary.Base64;

import com.google.gson.Gson;
import com.google.gson.JsonParser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JsonWebSignatureImpl implements JsonWebSignature {
    private static final Logger logger = LoggerFactory.getLogger(JsonWebSignatureImpl.class);
    private final String hashAlg;
    private final String keyAlg;
    private final byte[] header;
    private final byte[] serializedHeader;
    private final byte[] payload;
    private final byte[] serializedPayload;
    private final byte[] signature;
    private final byte[] serializedSignature;

    private static byte[] encodeString(String s) {
        try {
            return s.getBytes(StandardCharsets.UTF_8);
        } catch (Exception e) {
            logger.error("Exception in encodeString", e);
        }
        return s.getBytes();
    }

    private static String decodeString(byte[] buf) {
        if (buf == null || buf.length == 0) return "";
        try {
            return new String(buf, StandardCharsets.UTF_8);
        } catch (Exception e) {
            logger.error("Exception in decodeString", e);
        }
        return new String(buf);
    }

    public JsonWebSignatureImpl(String payload, PrivateKey privateKey) throws JwsException {
        this(encodeString(payload), privateKey);
    }

    public JsonWebSignatureImpl(byte[] payload, PrivateKey privateKey) throws JwsException {
        this.payload = payload;
        keyAlg = privateKey.getAlgorithm();
        if ("RSA".equals(keyAlg)) {
            hashAlg = "SHA256";
            header = encodeString("{\"alg\":\"RS256\"}");
        } else if ("DSA".equals(keyAlg)) {
            hashAlg = "SHA256";
            header = encodeString("{\"alg\":\"DS256\"}");
        } else {
            throw new IllegalArgumentException("Unsupported key algorithm " + keyAlg);
        }
        serializedHeader = Base64.encodeBase64URLSafe(header);
        serializedPayload = Base64.encodeBase64URLSafe(payload);
        try {
            Signature sig = Signature.getInstance(hashAlg + "with" + keyAlg);
            sig.initSign(privateKey);
            sig.update(serializedHeader);
            sig.update((byte)'.');
            sig.update(serializedPayload);
            signature = sig.sign();
            serializedSignature = Base64.encodeBase64URLSafe(signature);
        } catch (Exception e) {
            throw new JwsException("Error creating JWS", e);
        }
    }

    public JsonWebSignatureImpl(String serialization) throws JwsException {
        if (isCompact(serialization)) {
            try {
                String[] dotSeparatedParts = serialization.split("\\.");
                serializedHeader = encodeString(dotSeparatedParts[0]);
                header = Base64.decodeBase64(serializedHeader);
                serializedPayload = encodeString(dotSeparatedParts[1]);
                payload = Base64.decodeBase64(serializedPayload);
                serializedSignature = encodeString(dotSeparatedParts[2]);
                signature = Base64.decodeBase64(serializedSignature);
            } catch (Exception e) {
                throw new JwsException("Couldn't parse JWS", e);
            }
        } else {
            Gson gson = new Gson();
            JsonWebSignatureJsonSerialization jwsjs = gson.fromJson(serialization, JsonWebSignatureJsonSerialization.class);
            serializedHeader = encodeString(jwsjs.signatures.get(0).protectedPart);
            header = Base64.decodeBase64(serializedHeader);
            serializedPayload = encodeString(jwsjs.payload);
            payload = Base64.decodeBase64(serializedPayload);
            serializedSignature = encodeString(jwsjs.signatures.get(0).signature);
            signature = Base64.decodeBase64(serializedSignature);
        }
        String algString = getAlgStringFromHeader(header);
        keyAlg = getKeyAlgFromAlg(algString);
        hashAlg = getHashAlgFromAlg(algString);
    }

    private static String getAlgStringFromHeader(byte[] header) throws JwsException {
        try {
            return JsonParser.parseString(decodeString(header))
                .getAsJsonObject()
                .get("alg")
                .getAsString();
        } catch (Exception e) {
            throw new JwsException("Couldn't parse JWS header", e);
        }
    }

    private static String getKeyAlgFromAlg(String alg) throws JwsException {
        if (alg.startsWith("RS")) return "RSA";
        else if (alg.startsWith("DS")) return "DSA";
        throw new JwsException("Couldn't parse JWS header");
    }

    private static String getHashAlgFromAlg(String alg) throws JwsException {
        if (alg.endsWith("256")) return "SHA256";
        else if (alg.endsWith("160") || alg.endsWith("128") || alg.equals("DSA") || alg.equals("DS")) return "SHA1";
        else if (alg.endsWith("384")) return "SHA384";
        else if (alg.endsWith("512")) return "SHA512";
        throw new JwsException("Couldn't parse JWS header");
    }

    private static boolean isCompact(String serialization) {
        return !serialization.trim().startsWith("{");
    }

    @Override
    public String getPayloadAsString() {
        return decodeString(payload);
    }

    @Override
    public byte[] getPayloadAsBytes() {
        return payload.clone();
    }

    @Override
    public boolean validates(PublicKey publicKey) throws JwsException {
        if (!keyAlg.equals(publicKey.getAlgorithm())) return false;
        try {
            Signature sig = Signature.getInstance(hashAlg + "with" + publicKey.getAlgorithm());
            sig.initVerify(publicKey);
            sig.update(serializedHeader);
            sig.update((byte)'.');
            sig.update(serializedPayload);
            return sig.verify(signature);
        } catch (Exception e) {
            throw new JwsException("Error validating JWS", e);
        }
    }

    @Override
    public String serialize() {
        return decodeString(serializedHeader) +
                '.' +
                decodeString(serializedPayload) +
                '.' +
                decodeString(serializedSignature);
    }

    @Override
    public String serializeToJson() {
        String headerEncoded = decodeString(serializedHeader);
        String payloadEncoded = decodeString(serializedPayload);
        String signatureEncoded = decodeString(serializedSignature);
        return "{\"payload\":\"" + payloadEncoded + "\",\"signatures\":[{\"protected\":\"" + headerEncoded + "\",\"signature\":\"" + signatureEncoded + "\"}]}";
    }

}
